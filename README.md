Use to read From: field of new incoming emails. 
Good to know whether it makes sense to get up out of bed.

Copy newmail.sh to $PATH dir, like
sudo cp newmail.sh /usr/local/bin
chmod +x /usr/local/bin/newmail.sh

claws-mail -> configuration -> filtering
add filter with:
condition: unread | new
action: execute "newmail.sh message from %f"


and put that rule at the bottom of filters (if you have any spam filters, placing at bottom should skip reading spammers from fields"


OR copy 
matcherrc to ~/.claws-mail
